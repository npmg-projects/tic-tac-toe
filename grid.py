import pygame
import os

x_img = pygame.image.load(os.path.join('assets', 'x.png'))
o_img = pygame.image.load(os.path.join('assets', 'o.png'))


class Grid:
    def __init__(self):
        self.grid_lines = [((0, 200), (600, 200)),  # first horizontal
                           ((0, 400), (600, 400)),  # second horizontal
                           ((200, 0), (200, 600)),  # first vertical
                           ((400, 0), (400, 600))]  # second vertical
        self.grid = [[0 for x in range(3)] for y in range(3)]
        self.switch_player = True

    def get_cell_value(self, x, y):
        return self.grid[y][x]

    def set_cell_value(self, x, y, value):
        self.grid[y][x] = value

    def game_over(self, x, y):
        # check for row
        if self.grid[y][0] == self.grid[y][1] == self.grid[y][2]:
            return self.grid[y][0]
        # check for col
        if self.grid[0][x] == self.grid[1][x] == self.grid[2][x]:
            return self.grid[0][x]
        # check for prime diagonal
        if self.grid[0][0] == self.grid[1][1] == self.grid[2][2]:
            return self.grid[0][0]
        # check for the other diagonal
        if self.grid[2][0] == self.grid[1][1] == self.grid[0][2]:
            return self.grid[2][0]

        return -1

    def conquer_cell(self, x, y, player):
        if self.get_cell_value(x, y) == 0:  # check if cell is available to conquer
            self.switch_player = True
            if player == 'X':
                self.set_cell_value(x, y, 'x')
            elif player == 'O':
                self.set_cell_value(x, y, 'o')
        else:
            self.switch_player = False

    def draw_game_field(self, surface):
        for line in self.grid_lines:
            pygame.draw.line(surface, (200, 200, 200), line[0], line[1], 2)

        # draw the letters to their correct position
        for y in range(len(self.grid)):
            for x in range(len(self.grid[y])):
                if self.get_cell_value(x, y) == 'x':
                    surface.blit(x_img, (x * 200, y * 200))
                elif self.get_cell_value(x, y) == 'o':
                    surface.blit(o_img, (x * 200, y * 200))
