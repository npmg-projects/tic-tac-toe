import random

import pygame

import os

from grid import Grid

os.environ['SDL_VIDEO_WINDOWS_OS'] = '400, 100'

surface = pygame.display.set_mode((600, 600))
pygame.display.set_caption('Морски шах. Йей!')

running = True
grid = Grid()
background_color = [(0, 0, 0), (4, 26, 115), (58, 6, 137)]
surface.fill(random.choice(background_color))
player = 'X'
moves = 0
res_file = open('results.txt', 'a')
pygame.mixer.init()
click_sound_effect = pygame.mixer.Sound('click.mp3')


while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            pygame.mixer.Sound.play(click_sound_effect)
            if pygame.mouse.get_pressed()[0]:
                position = pygame.mouse.get_pos()
                grid.conquer_cell(position[0] // 200, position[1] // 200, player)
                if grid.switch_player:
                    moves += 1
                if grid.switch_player:
                    if player == 'X':
                        player = 'O'
                    else:
                        player = 'X'
                if moves >= 5:
                    winner = grid.game_over(position[0] // 200, position[1] // 200)
                    if not winner == -1:
                        res_file.write(f'{winner} wins \n')
                        running = False
                if moves == 9:
                    res_file.write('ooops! it is a draw. better luck next time!')
                    running = False
                print(grid.grid)

    grid.draw_game_field(surface)

    pygame.display.flip()
